### Hierarchical Data - Nested set model (MySQL Implementation) ###

The nested set model, also called modified preorder tree traversal, is another approach for handling hierarchical data. Instead of parent-child relations, hierarchy is represented as a set of nested containers by assigning left and right numerical values to each node

### Table Schema ###

```javascript
CREATE TABLE `category` (
  `id` int(10) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `left_leaf` int(11) NOT NULL,
  `right_leaf` int(11) NOT NULL
)
```

### Inserting Node (IF No Parent)###
> Select maximum `right_leaf` value

```javascript
SELECT
  Category.rght AS `right_leaf`
FROM
  category Category
ORDER BY
  right_leaf DESC
LIMIT 1;

/*If parent_id is NULL and First Insert*/

INSERT
INTO
  `category`(
    `id`,
    `parent_id`,
    `title`,
    `left_leaf`,
    `right_leaf`
  )
VALUES(NULL, NULL, 'Category Title', '1', '2');

/*Otherwise*/

UPDATE 
  category 
SET 
  left_leaf = (
    (left_leaf + 2)
  ) 
WHERE 
  (left_leaf >= <CATEGORY_RIGHT_LEAF>);
  
UPDATE 
  category 
SET 
  right_leaf = (
    (right_leaf + 2)
  ) 
WHERE 
  (right_leaf >= <CATEGORY_RIGHT_LEAF>);

INSERT
INTO
  category(
  `parent_id`,
  `title`,
  `left_leaf`,
  `right_leaf`)
VALUES(null,, 'Category Title', <CATEGORY_RIGHT_LEAF>, <CATEGORY_RIGHT_LEAF>+1)
```

### Inserting Node (IF Parent)###

```javascript
SELECT
  Category.parent_id AS `parent_id`,
  Category.left_leaf AS `left_leaf`,
  Category.left_right AS `left_right`
FROM
  category Category
WHERE
  Category.id = <CATEGORY_ID_HERE>
LIMIT 1;

UPDATE 
  category 
SET 
  left_leaf = (
    (left_leaf + 2)
  ) 
WHERE 
  (left_leaf >= <CATEGORY_RIGHT_LEAF>);
  
UPDATE 
  category 
SET 
  right_leaf = (
    (right_leaf + 2)
  ) 
WHERE 
  (right_leaf >= <CATEGORY_RIGHT_LEAF>);
  
INSERT
INTO
  category(
  `parent_id`,
  `title`,
  `left_leaf`,
  `right_leaf`)
VALUES(<CATEGORY_ID_HERE>, 'Category Title', <CATEGORY_RIGHT_LEAF>, <CATEGORY_RIGHT_LEAF>+1)
```